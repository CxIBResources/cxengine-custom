FROM registry1.dso.mil/ironbank/checkmarx/cxlite/cxengine:9.3

USER root
RUN chmod a+rwx /cx_inst && \
    chmod a+rwx /var/run && \
    chmod a+rwx /var/log && \
    rm /var/run/redis_6379.pid && \
    rm /var/log/redis_6379.log
