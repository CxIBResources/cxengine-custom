#CxEngine-Custom Custom Image Template

Quick Start Guide
- Import this project into your organization's Gitlab instance
- Create and populate the following CI/CD Variables for this Project or Owning Group as described:
    - RO_USER_NAME <- Set to Registry1 User Name
    - RO_CLI_TOKEN <- Set to Registry1 CLI Token
- Update placeholder values in 'gitlab-ci-sample.yml' as described: 
    !! (Note: Values should be formatted in lowercase, with spaces replaced by dashes) !!
    - {LCASE_PROJECT_NAMESPACE} <- Replace placeholder, including {}, with name of project namespace (Use name in URL value)
    - {LCASE_IMAGE_NAME}        <- Replace placeholder, including {}, with name of target container image (Use name in URL slug)
- Create new '.gitlab-ci.yml' file with contents of 'gitlab-ci-sample.yml' (Don't forget the period in front of .gitlab-ci.yml!)
- Allow Pipeline to complete
- Review newly created CxEngine custom image in 'Packages & Registries'>'Container Registry'
